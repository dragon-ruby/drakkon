module Drakkon
  # General Entrypoint for CLI
  module CLI
    # Entrypoint for the CLI
    def self.init
      # Home Directory Handling
      Hub.init

      # Split up CLI Args
      cmd = ARGV[0]
      args = ARGV[1..]

      start(cmd&.to_sym, args)
    end

    def self.start(cmd, args)
      case cmd
      when :init then Init.go!(args)
      when :setup then Setup.go!(args)
      when :run then Run.go!(args)
      when :build then Build.go!(args)
      when :publish then Build.go!([], '')
      when :images then Images::CLI.run!(args)
      when :utils then Utils::CLI.run!(args)
      when :install then Version.install!(args)
      when :gem then Gems::CLI.init!(args)
      when :skeleton then Skeleton::CLI.init!(args)
      when :versions then Version.list
      when :console then console
      when :web then Web.run!(args)
      when :exit then exit(0)
      else
        start(menu, args)
      end
    end

    def self.prompt
      TTY::Prompt.new(active_color: :cyan, interrupt: :exit)
    end

    def self.console
      binding.pry
    end

    def self.menu
      prompt.select('Wat do?', filter: true, per_page: 100) do |menu|
        menu.choice name: 'run', value: :run
        menu.choice name: 'setup (config/initialization)', value: :setup
        menu.choice name: 'build', value: :build
        menu.choice name: 'publish', value: :publish
        menu.choice name: 'install (DragonRuby Versions)', value: :install
        menu.choice name: 'Image Helpers [images]', value: :images
        menu.choice name: 'Utilities [utils]', value: :utils
        menu.choice name: 'gem (Shared Code)', value: :gem
        menu.choice name: 'skeleton (Templates)', value: :skeleton
        menu.choice name: 'console (pry)', value: :console
        menu.choice name: 'exit', value: :exit
      end
    rescue TTY::Reader::InputInterrupt
      exit 0
    end

    #=======================================================
  end
  #=======================================================
end
