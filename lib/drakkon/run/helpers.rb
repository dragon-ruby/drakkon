module Drakkon
  # Run Command for CLI
  module Run
    extend PlatformCompat

    def self.index
      @index ||= build_index
      @index
    end

    def self.build_index
      Commands.public_methods.select { |x| x.to_s[0..3] == 'cmd_' }.map do |cmd|
        cmd.to_s.gsub('cmd_', '')
      end
    end

    def self.readline_notch
      "#{'drakkon'.pastel(:bright_black)} » "
    end

    # Exec skips the weird shell stuff
    def self.run_cmd
      ['./dragonruby', @context, *args]
      # "PATH=#{version_dir}:$PATH exec dragonruby #{@context} > #{log_file}"
    end

    # No longer used
    # def self.log_file
    #   "#{Hub.dir}/log.txt"
    # end

    def self.dragonruby(value = nil)
      @dragonruby ||= value

      @dragonruby
    end

    def self.args(raw = [])
      @args ||= raw

      @args
    end

    def self.version_dir
      "#{Hub.dir}/#{Settings.version}"
    end

    def self.force_images?
      args.include?('images')
    end

    def self.force_manifest?
      args.include?('manifest')
    end

    def self.force_fonts?
      args.include?('fonts')
    end

    def self.force_sounds?
      args.include?('sounds')
    end

    def self.prompt
      TTY::Prompt.new(active_color: :cyan, interrupt: :exit)
    end

    #=======================================================
  end
  #=======================================================
end
