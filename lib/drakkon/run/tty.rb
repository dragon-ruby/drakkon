module Drakkon
  # Run Command for CLI
  module Run
    # Command Processing
    def self.run
      return true if @list.empty?

      cmd = @list.shift
      if index.include? cmd
        Commands.send(:"cmd_#{cmd}", @list)
      else
        puts "Unknown Command: '#{cmd.pastel(:red)}'"
        puts
        puts "Available Commands: #{index.join(', ').pastel(:green)}"
      end
    rescue StandardError => e
      LogBot.fatal('CLI Run', e.message)
      puts e.backtrace[0..4].join("\n").pastel(:red)
    end

    # CLI Start
    def self.cli
      # Initial Log Follow
      Commands.send(:cmd_tail, [])

      value ||= '' # Empty Start

      loop do
        line = reader.read_line(readline_notch, value: value)
        value = '' # Remove Afterwards

        if reader.breaker
          value = line
          puts ''
          next
        end

        break if line =~ /^exit/i

        process
        run
      end
    end

    def self.reader
      @reader ||= reader_setup
    end

    def self.reader_setup
      reader = TTY::Reader.new(history_duplicates: false, interrupt: -> { back })

      # Blank?
      reader.add_to_history ''

      # Remove Line
      reader.on(:keyctrl_u) do |_event|
        reader.line.remove reader.line.text.size
      end

      # Navigate Word Left
      reader.on(:keyctrl_left) { reader.line.move_word_left }

      # Navigate Word Right
      reader.on(:keyctrl_right) { reader.line.move_word_right }

      # Navigate Beginning
      reader.on(:keyctrl_a) { reader.line.move_to_start }

      # Navigate End
      reader.on(:keyctrl_e) { reader.line.move_to_end }

      reader.on(:keyback_tab) { back }

      # TODO: Keytab?
      # reader.on(:keytab) do
      #   process
      #   auto
      # end

      reader.instance_variable_get(:@history)

      # DEBUG PRY
      reader.on(:keyctrl_p) do |event|
        binding.pry
        # rubocop:enable Lint/Debugger
      end

      reader
    end

    def self.back
      # puts 'clear'
      raise Interrupt
    end

    # Auto/Run Process - Populate and parse: @list
    def self.process
      line = reader.line
      @list = Shellwords.split line.text
    rescue StandardError => e
      puts "#{'Invalid Command'.pastel(:red)}: #{e.message.pastel(:green)}"
    end

    def self.cursor
      @cursor ||= TTY::Cursor

      @cursor
    end

    def self.logs
      @logs ||= []

      @logs
    end

    #=======================================================
  end
  #=======================================================
end
