module Drakkon
  # Run Command for CLI
  module Run
    # Runnable Terminal Commands
    module Commands
      def self.cmd_images(_args)
        Images::Index.run!(force: true, dir: Run.context)
      end

      # ========================================================================
    end
    # ==========================================================================
  end
end
