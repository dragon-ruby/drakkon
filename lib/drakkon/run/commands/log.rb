module Drakkon
  # Run Command for CLI
  module Run
    # Runnable Terminal Commands
    module Commands
      def self.cmd_logs(_args)
        TTY::Pager.page(command: 'less +G') do |pager|
          Run.logs.each do |line|
            pager.write line
          end
        end
      end

      def self.cmd_tail(_args)
        loop do
          if Run.dragonruby.ready?
            $stdin.cooked!
            line = Run.dragonruby.readline
            Run.logs.push line # Store
            puts line
          elsif key_pressed?
            key = $stdin.readchar
            if key.ord >= 32 && key.ord <= 126
              break
            else
              break if key == "\u0003" # 'CTRL C'
              break if key == "\u0004" # 'CTRL D'

              $stdin.cooked!
              puts "any key to stop following: #{key.inspect}".pastel(:green)
            end

          else
            $stdin.raw!
            sleep 0.1
          end
        end

        # Restore the previous raw mode
        $stdin.cooked!
      rescue EOFError
        :ignore
      end

      def self.key_pressed?
        # Set raw mode to ensure immediate input
        $stdin.raw(&:ready?)
      ensure
        # Restore the previous raw mode
        $stdin.cooked!
      end

      # ========================================================================
    end
    # ==========================================================================
  end
end
