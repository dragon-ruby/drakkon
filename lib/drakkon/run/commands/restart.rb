module Drakkon
  # Run Command for CLI
  module Run
    # Runnable Terminal Commands
    module Commands
      def self.cmd_restart(_args)
        Run.restart
      end

      # ========================================================================
    end
    # ==========================================================================
  end
end
