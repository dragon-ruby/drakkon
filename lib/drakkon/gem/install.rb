module Drakkon
  module Gems
    # Run Command for CLI
    module Install
      def self.opts
        @opts
      end

      def self.start(args)
        gem = Gems::Gem.new
        gem.intall_setup(args)
      rescue TTY::Reader::InputInterrupt
        exit 0
      end

      def self.prompt
        TTY::Prompt.new(active_color: :cyan, interrupt: :exit)
      end

      #=======================================================
    end
    #=======================================================
  end
end
