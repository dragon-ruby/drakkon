module Drakkon
  module Gems
    # Gem file manipulation and selection
    module GemHelpers
      def select_files
        data[:files] ||= {}
        file_edit
      end

      def file_edit
        # Select File
        file = prompt_file
        file_id = file.to_sym

        if data[:files].key?(file_id) && prompt.yes?("Remove File? #{file.pastel(:bright_blue)}")
          data[:files].delete file_id
          return
        end

        # Fine Weight
        weight = prompt.ask("File: #{file.pastel(:bright_blue)}, What weight?", convert: :integer, default: 10)

        # Datas
        file_data = {
          weight: weight,
          file: file
        }

        # Save
        data[:files][file_id] = file_data
      end

      def path_files
        Dir["#{data[:path]}/**/*.rb"].map { |x| x.gsub("#{data[:path]}/", '') }
      end

      def prompt_file
        prompt.select('Select File:', filter: true, per_page: 25) do |menu|
          path_files.each do |file|
            name = if data[:files].key? file.to_sym
                     "#{file.pastel(:bright_blue)} #{'✔'.pastel(:bright_green)}"
                   else
                     file
                   end

            menu.choice name: name, value: file
          end
        end

        # Return
      end

      # =============================================================
    end
    # =============================================================
  end
end
