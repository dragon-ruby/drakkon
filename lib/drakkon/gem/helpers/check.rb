module Drakkon
  module Gems
    # General Helpers for Gem Class
    module GemHelpers
      def do_the_check(installing: true)
        unless File.exist? config_file
          LogBot.fatal('Gem', "Drakkon config file not found! #{config_file}")
          exit 0
        end

        # Validate Config Structure
        read_config

        # Valid Structure - Name
        valid_structure?

        # Validate / Find Valid Modules
        load_modules

        # Check for Conflict
        return if installing == false

        if Settings.config[:gems].key?(name)
          LogBot.fatal('Gem', 'Duplicate Gem already installed')
          exit 0
        end

        # I hate the other syntax
        :return
      end

      def valid_structure?
        unless config.key? :name
          LogBot.fatal('Gem', 'Name not found!')
          exit 0
        end

        @name = config[:name].to_sym

        # Validate Versioning
        valid_version?
      end

      def valid_version?
        unless config.key? :version
          LogBot.fatal('Gem', 'Version not found')
          exit 0
        end

        Semantic::Version.new config[:version]
      rescue StandardError => e
        LogBot.fatal('Gem',
                     "Invalid Version: #{config[:version].pastel.to_s.pastel(:green)}; #{e.message.pastel(:yellow)}")
        exit 0
      end

      def load_modules
        @modules = config[:modules].select do |mod|
          valid_mod?(mod)
        end

        return unless module_duplicates?

        LogBot.fatal('Gem', "Duplicate Modules: #{module_duplicates}")
        exit 0
      end

      def valid_mod?(mod)
        unless mod.key?(:name)
          LogBot.fatal('Gem', "Invalid Mod. No name found: #{mod}")
          return false
        end

        unless files_exist?(mod[:files])
          LogBot.fatal('Gem', "Invalid Mod: Not all files found: #{mod}")
          return false
        end

        true
      end

      def files_exist?(files)
        files.all? do |file|
          if File.exist?("#{data[:path]}/#{file}.rb")
            true
          else
            LogBot.fatal('Gem', "Missing File: #{file.pastel(:red)}")
            exit 1
          end
        end
      end

      def module_index
        modules.map(&:name)
      end

      # Helper to make easier to identify dupes
      def module_duplicates
        module_index.tally.select { |_, count| count > 1 }.keys
      end

      def module_duplicates?
        module_index.count != module_index.uniq.count
      end

      # =============================================================
    end
    # =============================================================
  end
end
