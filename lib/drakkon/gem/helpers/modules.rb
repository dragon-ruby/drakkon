module Drakkon
  module Gems
    # General Helpers for Gem Class
    module GemHelpers
      def select_modules
        return if (data[:modules].nil? || data[:modules].empty?) && modules.empty?

        data[:modules] = prompt_modules.map do |selected|
          modules.find { |x| x[:name] == selected }
        end
      end

      def refresh_modules
        read_config
        valid_structure?
        load_modules

        # Ignore if no modules
        return if data[:modules].nil? || data[:modules].empty?

        data[:modules] = data[:modules].map(&:name).map do |selected|
          modules.find { |x| x[:name] == selected }
        end
      end

      def prompt_modules
        prompt.multi_select('Select Modules:', filter: true, per_page: modules.count) do |menu|
          # menu.default *data[:modules].map(&:name) if data[:modules]
          # menu.default 'Core - Daemon Core Modules'

          defaults = []

          modules.each do |mod|
            # Format
            name = mod.name.pastel(:bright_blue)
            name += " - #{mod[:description]}".pastel(:bright_black) if mod.key?(:description)

            # Preselect Previous ones if exists
            defaults.push(name) if data[:modules]&.any? { |x| x[:name] == mod&.name }

            # Choice
            menu.choice name: name, value: mod[:name]
          end

          # Defaults
          menu.default(*defaults)
        end

        # Return
      end

      # =============================================================
    end
    # =============================================================
  end
end
