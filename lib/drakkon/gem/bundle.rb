module Drakkon
  module Gems
    # Run Command for CLI
    module Bundle
      def self.build!(args, context)
        # Gems are not required
        # return if Settings.gems.empty?

        @context = context

        if Settings.config[:bundle_digest] == digest && !args.include?('bundle') && File.exist?(bundle_file)
          LogBot.info('Gems Bundle', 'Nothing New')
          return unless args.include?('bundle')
        else
          LogBot.info('Gems Bundle', 'Bundling')
        end

        idx = collect

        if Settings.bundle_compile?
          LogBot.info('Gems Bundle', 'Compile')
          compiled_bundle(idx)
        else
          LogBot.info('Gems Bundle', 'Expand')
          expand_bundle(idx)
        end

        Settings.update(:bundle_digest, digest)
      rescue TTY::Reader::InputInterrupt
        exit 0
      end

      # Including files without merging them
      def self.expand_bundle(idx)
        # Make Directory `app/drakkon` if it doesn't exist
        FileUtils.mkdir_p('app/drakkon') unless File.directory?('app/drakkon')

        # Create Sub Directory to make switching to compile / eaiser
        # Always clear to ensure fresh directory
        FileUtils.rm_rf(unbundle_dir) if File.directory?(unbundle_dir)
        FileUtils.mkdir_p(unbundle_dir)

        # Touch the file if it doesn't exist
        FileUtils.touch(bundle_file) unless File.exist?(bundle_file)

        File.open(bundle_file, 'w') do |f|
          idx.keys.sort.reverse.each do |i|
            idx[i].each do |file_data|
              file = file_data[:path]
              unless File.exist?(file)
                LogBot.fatal('Bundle', "File not found #{file.pastel(:red)}")
                next
              end

              file_path = "#{unbundle_dir}/#{File.basename(file_data[:safe_name])}.rb"

              FileUtils.cp(file, file_path)
              f << "require '#{file_path}'\n"
            end
          end

          # Include other files
          f << "require 'app/drakkon/image_index.rb' \n" if Settings.image_index?
          f << "require 'app/drakkon/font_index.rb' \n" if Settings.font_index?
          f << "require 'app/drakkon/sound_index.rb' \n" if Settings.sound_index?
          f << "require 'app/drakkon/manifest.rb' \n" if Settings.manifest?

          # File Close
        end

        # FileUtils.cp(bundle_file, "#{drakkon_dir}/bundle/#{Time.now.to_i}.rb")
      end

      def self.compiled_bundle(idx)
        # Always clear to ensure fresh directory
        FileUtils.rm_rf(unbundle_dir) if File.directory?(unbundle_dir)

        # Make Directory `app/drakkon` if it doesn't exist
        FileUtils.mkdir_p('app/drakkon') unless File.directory?('app/drakkon')

        # Touch the file if it doesn't exist
        FileUtils.touch(bundle_file) unless File.exist?(bundle_file)

        File.open(bundle_file, 'w') do |f|
          # Index Writer
          idx.keys.sort.reverse.each do |i|
            idx[i].each do |file_data|
              file = file_data[:path]

              unless File.exist?(file)
                LogBot.fatal('Bundle', "File not found #{file.pastel(:red)}")
                next
              end
              f << File.read(file)
              f << "\n"
            end
          end

          # Include other files
          f << "require 'app/drakkon/image_index.rb' \n" if Settings.image_index?
          f << "require 'app/drakkon/font_index.rb' \n" if Settings.font_index?
          f << "require 'app/drakkon/sound_index.rb' \n" if Settings.sound_index?
          f << "require 'app/drakkon/manifest.rb' \n" if Settings.manifest?

          # File Close
        end
      end

      def self.collect
        idx = {}
        Settings.gems.each do |gem_name, gem|
          gem[:name] = gem_name.to_s.downcase
          case gem[:source].to_sym
          when :local then collect_local_source(idx, gem)
          else
            LogBot.fatal('Bundle', "Invalid Source: #{gem[:source].pastel(:red)}")
          end
        end

        idx
      end

      def self.collect_local_source(idx, gem)
        path = gem[:data][:path]

        # Modules
        if gem[:data].key?(:modules)
          gem[:data][:modules].each do |mod|
            idx[mod.weight] ||= []
            mod.files.each do |file|
              file_name = "#{path}/#{file}.rb"
              unless File.exist?(file_name)
                LogBot.fatal('Bundle',
                             "Module File not found #{mod.name.pastel(:bright_blue)}, #{file_name.pastel(:red)}")
                next
              end

              # Convert full file path into single string / relative naming (no directories)
              safe_name = "#{gem.name}_#{file.gsub('/', '_')}"

              idx[mod.weight].push(path: file_name, name: file, safe_name: safe_name)
            end
          end
        end

        # Files
        if gem[:data].key?(:files)
          gem[:data][:files].each_value do |f|
            idx[f.weight] ||= []
            # idx[f.weight].push "#{path}/#{f.file}"
            safe_name = "#{gem.name}_#{f.file.gsub('/', '_')}"

            idx[f.weight].push(path: "#{path}/#{f.file}", name: f.file, safe_name: safe_name)
          end

        end

        # Return
      end

      # ====================================================
      # General Helpers
      # ====================================================
      def self.bundle_file
        "#{drakkon_dir}/bundle.rb"
      end

      def self.drakkon_dir
        "#{@context}/app/drakkon"
      end

      # Directory for uncompiled files / Relative
      def self.unbundle_dir
        'app/drakkon/unbundle'
      end

      def self.prompt
        TTY::Prompt.new(active_color: :cyan, interrupt: :exit)
      end

      def self.digest
        Digest::MD5.hexdigest Settings.gems.to_s
      end

      #=======================================================
    end
    #=======================================================
  end
end
