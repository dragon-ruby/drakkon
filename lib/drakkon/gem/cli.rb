module Drakkon
  module Gems
    # Run Command for CLI
    module CLI
      def self.args(raw = [])
        @args ||= raw

        @args
      end

      # General Run
      def self.init!(raw)
        Settings.ready?

        args(raw)
        cmd = args.shift

        start(cmd&.to_sym)
      end

      def self.start(cmd)
        case cmd
        when :install then Gems::Install.start(args)
        when :configure then Gems::Configure.start(args)

        else
          start(menu)
        end
      end

      def self.prompt
        TTY::Prompt.new(active_color: :cyan, interrupt: :exit)
      end

      def self.menu
        prompt.select('Wat do?', filter: true) do |menu|
          menu.choice name: 'install (New Gem)', value: :install
          menu.choice name: 'configure', value: :configure unless Settings.gems.empty?
        end
      rescue TTY::Reader::InputInterrupt
        exit 0
      end

      #=======================================================
    end
    #=======================================================
  end
end
