module Drakkon
  # Run Command for CLI
  module Build
    extend PlatformCompat

    def self.args(raw = nil)
      @args ||= raw

      @args
    end

    # General Run
    # rubocop:disable Metrics/AbcSize
    def self.go!(raw, publish = '--package')
      Settings.ready?
      args(raw)

      Settings.update(:platforms, platform_setup) unless Settings.platforms?

      # Run Index if configured
      Manifest.index if Settings.config[:manifest]

      builder = if args.empty?
                  Settings.platforms
                else
                  platforms.select { |i| args.any? { |a| i.include? a } }
                end

      if builder.empty?
        LogBot.fatal('Build',
                     "No valid platforms found! #{args.inspect.pastel(:red)} : #{builder.inspect.pastel(:red)}")
        exit(0)
      end

      list = builder.join(',')
      LogBot.info('Build', "Building for platforms: #{list}")

      # Save Current Directory before changing to Version Directory
      context = Dir.pwd
      context_name = File.basename(context)
      version = metadata_version(context)

      # Yes... build.build! :D
      Dir.chdir(version_dir) do
        # Clean Up Accidental Leftovers
        FileUtils.rm_r(context_name) if Dir.exist? context_name

        if Settings.manifest_compile?
          manifest_compile(context, context_name)
        else
          # DragonRuby Publish doesn't like symlinks or PATH...
          FileUtils.cp_r(context, context_name)
        end

        # Clean Builds Dir
        FileUtils.rm_r(Dir['builds/*'])

        # Remove Project's Builds Directory / Copied Version
        FileUtils.rm_r("#{context_name}/builds") if Dir.exist? "#{context_name}/builds"

        # Execute
        io = IO.popen(build_env, build!(list, context_name, publish))
        output(io)

        # Preflight
        FileUtils.mkdir_p("#{context}/builds")
        FileUtils.mkdir_p("#{context}/builds/#{version}")

        # Copy back to Project Dir
        FileUtils.cp_r(Dir['builds/*'], "#{context}/builds/#{version}/")

        # Copy Settings
        copy = Settings.config.dig(:builds, :copy)

        if copy
          FileUtils.mkdir_p("#{copy}/#{version}")
          FileUtils.cp(Dir['builds/*.zip'], "#{copy}/#{version}/")
        end

        # Clean Up
        FileUtils.rm_r(context_name)
      end
    end
    # rubocop:enable Metrics/AbcSize

    # Send output to console
    def self.output(io)
      loop do
        next unless io.ready?

        puts io.readline
      end
    rescue SystemExit, Interrupt, EOFError
      LogBot.info('Build', 'Exiting')
    end

    def self.manifest_compile(context, context_name)
      # Base Folders
      Dir.mkdir context_name
      Dir.mkdir "#{context_name}/app"

      # Copy Directories that are not being managed
      FileUtils.cp_r("#{context}/fonts", "#{context_name}/fonts")
      FileUtils.cp_r("#{context}/sprites", "#{context_name}/sprites")
      FileUtils.cp_r("#{context}/sounds", "#{context_name}/sounds")
      FileUtils.cp_r("#{context}/metadata", "#{context_name}/metadata")

      # Copy over base files
      FileUtils.cp("#{context}/app/main.rb", "#{context_name}/app/main.rb")
      FileUtils.cp_r("#{context}/app/drakkon", "#{context_name}/app/drakkon")

      # Compile into Manifest
      File.open("#{context_name}/app/drakkon/manifest.rb", 'w') do |f|
        Manifest.index.reverse.each do |file|
          f << File.read("#{context}/#{file}")
        end
      end
    end

    def self.metadata_version(context)
      File.read("#{context}/metadata/game_metadata.txt").split("\n").find { |y| y.include? 'version' }.split('=').last
    end

    # DEW THE BUILD
    def self.build!(list, context, package = '--package')
      # 5.25 not liking the blank package flag
      cmd = []
      cmd.push './dragonruby-publish'
      cmd.push package unless package.nil? || package.empty?
      cmd.push "--platforms=#{list}"
      cmd.push context

      cmd
    end

    def self.version_dir
      "#{Hub.dir}/#{Settings.version}"
    end

    def self.force_images?
      args.include?('images')
    end

    def self.force_fonts?
      args.include?('fonts')
    end

    def self.force_sounds?
      args.include?('sounds')
    end

    def self.force_manifest?
      args.include?('manifest')
    end

    def self.platform_setup
      prompt.multi_select('Select Platforms to build for (these will be the future default):', platforms, filter: true)
    end

    def self.platforms
      %w[
        windows-amd64
        linux-amd64
        macos
        android-0
        android
        googleplay-0
        googleplay
        html5-0
        html5
        linux-raspberrypi
        mac-0
        oculusquest-0
        oculusquest
      ]
    end

    def self.prompt
      TTY::Prompt.new(active_color: :cyan, interrupt: :exit)
    end

    #=======================================================
  end
  #=======================================================
end
