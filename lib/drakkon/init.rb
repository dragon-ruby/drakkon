module Drakkon
  # Run Command for CLI
  module Init
    # General Run
    def self.go!(_raw = nil)
      if Settings.init?
        LogBot.info('Init', 'Already Configured!')
        exit(0)
      end

      run!
    rescue SystemExit, Interrupt
      LogBot.info('Init', 'Exiting')
      exit
    end

    def self.run!
      unless Hub.version_installed?
        LogBot.warn('Init', 'No Drakkon Versions Installed, Run `drakkon install <dragonruby.zip>` first!')
        exit(1)
      end

      exit if prompt.no?("Init Drakkon here? #{Dir.pwd.pastel(:yellow)}")
      LogBot.info('Init', 'Start')
      settings
      directories
      metadata

      # TODO: Validate / Confirm
      try_write('app/main.rb') do |to|
        File.write(to, main_tick)
      end
    end

    def self.try_write(to, &blk)
      if File.exist?(to)
        LogBot.warn('Init', "- Tried to create #{to} but it already exists")
      else
        LogBot.info('Init', "- Created #{to}")
        blk.call(to)
      end
    end

    def self.metadata
      # Icon is required for build
      try_write('metadata/icon.png') do |to|
        FileUtils.cp(metdata_dir('icon.png'), to)
      end

      # Copy metdata files
      try_write('metadata/game_metadata.txt') do |to|
        FileUtils.cp(metdata_dir('game_metadata.txt'), to)
      end
    end

    def self.metdata_dir(file_name = nil)
      "#{Run.version_dir}/mygame/metadata/#{file_name}"
    end

    def self.settings
      LogBot.info('Init', 'Settings Setup')

      # If we're here, it means Settings.init? returned false,
      # which eans the config file doesn't exist.
      # As a happy accident, accessing Settings.config for the first time also
      # writes out a default config file
      Settings.config
    end

    def self.directories
      LogBot.info('Init', 'Directories Setup')

      ['app', 'fonts', 'metadata', 'fonts', 'sounds', 'sprites', 'app/drakkon'].each do |dir|
        next if Dir.exist?(dir)

        Dir.mkdir dir
      end
    end

    def self.main_tick
      <<~MAIN
        require 'app/drakkon/bundle.rb'

        def tick(gtk_args)
          # args.gtk.slowmo! 2
          # =========================================================
        end
      MAIN
    end

    def self.prompt
      TTY::Prompt.new(active_color: :cyan, interrupt: :exit)
    end

    #=======================================================
  end
  #=======================================================
end
