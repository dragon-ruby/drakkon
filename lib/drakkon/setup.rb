module Drakkon
  # Run Command for CLI
  module Setup
    # General Run
    def self.go!(_raw = nil)
      Settings.ready?

      loop do
        Settings.menu_do Settings.menu
      end
    rescue SystemExit, Interrupt
      LogBot.info('Init', 'Exiting')
      exit
    end

    def self.prompt
      TTY::Prompt.new(active_color: :cyan, interrupt: :exit)
    end

    def self.basic_directories
      ['app', 'fonts', 'metadata', 'fonts', 'sounds', 'sprites', 'app/drakkon'].each do |dir|
        next if Dir.exist?(dir)

        Dir.mkdir dir
      end
    end

    #=======================================================
  end
  #=======================================================
end
