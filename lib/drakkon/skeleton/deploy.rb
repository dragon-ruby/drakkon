module Drakkon
  module Skeleton
    # Run Command for CLI
    module Deploy
      def self.start(args)
        name = args.shift&.to_sym
        name = skeleton_select if name.nil?

        template_name = args.shift
        template = collect_skeleton_templates(name.to_sym).find { |x| x[:name] == template_name } if template_name
        template = skeleton_template_select(name) if template.nil?

        variables = collect_variables(template)

        template[:files].each do |files|
          file_src = "#{template[:path]}/#{files[:source]}"
          unless File.exist?(file_src)
            LogBot.fatal('Skeleton', "Missing File! File: #{file_src}")
            exit 0
          end

          src = File.read(file_src)
          result = process_variables(src, variables)

          # Assume Src is Target Unless specified
          files[:target] ||= files[:source]

          # Allow for Dynamic Targets
          target = process_variables(files[:target], variables)

          # Map Names
          target = target.downcase if files[:downcase]

          # Create Subdirectories as needed
          FileUtils.mkdir_p File.dirname(target)
          File.write(target, result)
        end
      end

      def self.process_variables(src, variables)
        result = src.clone
        variables.each do |key, value|
          result.gsub!(/DRAKKON_#{key}/, value)
        end

        result
      end

      def self.collect_variables(template)
        variables = {}
        template[:variables].each do |var|
          question = var[:prompt] || var[:key]
          variables[var[:key].to_sym] = prompt.ask(question, **var[:ask])
        end

        variables
      end

      def self.skeleton_select
        prompt.select('Which Skeleton?', filter: true, per_page: 25) do |menu|
          Hub.skeletons.each_key { |sk|  menu.choice name: sk, value: sk }
        end
      rescue TTY::Reader::InputInterrupt
        exit 0
      end

      def self.collect_skeleton_templates(name)
        data = Hub.skeletons[name]
        Hub.skeletons[name][:templates].map do |template_name|
          template_config_file = case data[:source].to_sym
                                 when :local
                                   "#{data[:path]}/#{template_name}/skeleton.json"
                                 end

          template_config = JSON.parse(File.read(template_config_file), { symbolize_names: true })
          template_config[:path] = case data[:source].to_sym
                                   when :local
                                     "#{data[:path]}/#{template_name}"
                                   end

          template_config
        end
      end

      def self.skeleton_template_select(name)
        templates = collect_skeleton_templates(name)

        prompt.select('Which Template?', filter: true, per_page: 25) do |menu|
          templates.each do |template|
            name = template[:name].pastel(:bright_blue)
            name += " - #{template[:description]}".pastel(:bright_black)

            menu.choice name: name, value: template
          end
        end
      rescue TTY::Reader::InputInterrupt
        exit 0
      end

      def self.prompt
        TTY::Prompt.new(active_color: :cyan, interrupt: :exit)
      end

      #=======================================================
    end
    #=======================================================
  end
end
