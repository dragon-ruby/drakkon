module Drakkon
  module Skeleton
    # Run Command for CLI
    module CLI
      def self.args(raw = [])
        @args ||= raw

        @args
      end

      # General Run
      def self.init!(raw)
        args(raw)
        cmd = args.shift

        start(cmd&.to_sym)
      end

      def self.start(cmd)
        case cmd
        when :install then Skeleton::Install.new(args)
        when :deploy then Skeleton::Deploy.start(args)

        else
          start(menu)
        end
      end

      def self.prompt
        TTY::Prompt.new(active_color: :cyan, interrupt: :exit)
      end

      def self.menu
        prompt.select('Wat do?', filter: true) do |menu|
          menu.choice name: 'install (New Templates)', value: :install
          menu.choice name: 'deploy', value: :deploy if Hub.skeletons?
        end
      rescue TTY::Reader::InputInterrupt
        exit 0
      end

      #=======================================================
    end
    #=======================================================
  end
end
