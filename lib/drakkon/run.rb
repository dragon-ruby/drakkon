module Drakkon
  # Run Command for CLI
  module Run
    # General Run
    def self.go!(raw)
      Settings.ready?
      Settings.check_version!

      args(raw)

      # Save Current Directory before changing to Version Directory
      @context = Dir.pwd

      run_setup

      watcher!

      # Yes... Run.run!
      Dir.chdir(version_dir) do
        runtime
        cli
      end

      # Finish
    rescue SystemExit, Interrupt
      LogBot.info('Run', 'Exiting')
      puts
      exit
    ensure
      Process.kill('TERM', dragonruby&.pid) if dragonruby
      watcher&.kill
    end

    # Helper for external access
    def self.context
      @context ||= Dir.pwd

      @context
    end

    def self.watcher!
      @watcher = Thread.new { watch }
    end

    def self.watcher
      @watcher
    end

    def self.watch
      # ======================================
      # Watch for Updates
      # ======================================
      # Force the bundle
      files = Gems::Bundle.collect.values.flatten.map(&:path)
      files.push "#{Run.context}/sprites/" if Settings.image_index?

      Filewatcher.new(files, every: true).watch do |changes|
        sleep 1 # Sleep to prevent exceptions?
        Images::Index.run!(force: true, dir: Run.context) if changes.keys.find { |x| x.include? 'sprites/' }
        Gems::Bundle.build!(['bundle'], @context)
      end
    end

    def self.run_setup
      Images::Index.run!(force: force_images?) if Settings.image_index?
      Manifest.run!(force: force_manifest?) if Settings.manifest?
      Fonts::Index.run!(force: force_fonts?) if Settings.font_index?
      Sounds::Index.run!(force: force_sounds?) if Settings.sound_index?
      Gems::Bundle.build!(args, @context)
    end

    def self.runtime
      @dragonruby = IO.popen(run_env, run_cmd)
    end

    def self.restart
      Process.kill('TERM', dragonruby&.pid)
      runtime
    end

    #=======================================================
  end
  #=======================================================
end
