module Drakkon
  # Helper to provide platform compatibility
  module PlatformCompat
    def run_env
      {
        'PATH' => "#{version_dir}#{path_divider}#{ENV.fetch('PATH', nil)}"
      }
    end

    def build_env
      run_env # not sure if these will ever be different
    end

    def windows?
      (/cygwin|mswin|mingw|bccwin|wince|emx/ =~ RUBY_PLATFORM) != nil
    end

    def path_divider
      if windows?
        ';'
      # Linux uses ':' (same with mac/unix?)
      else
        ':'
      end
    end
  end
end
