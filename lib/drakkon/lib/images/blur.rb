module Drakkon
  module Images
    # General Image Index Helper
    module Blur
      def self.run!(args = [])
        # Sizing
        size = if args.empty?
                 prompt.ask('Blur Size? (e.g. 20x20): ', default: '5x5')
               else
                 args.shift
               end

        # TODO: Recommend as Options
        # TODO: Blur Options

        # Potentially Support Custom Filters?
        filter = if args.empty?
                   'Gaussian'
                 else
                   args.shift
                 end

        puts <<~HELP
                    Usage [size] [filter = Gaussian]
                    - Run in the directory you wish to modify the images
                    - Blurs the images via MiniMagick

          Filter: #{filter} to Size: #{size}
                    Current Directory;
                      #{Dir.pwd.pastel(:yellow)}

          Images:
        HELP

        images.sort.each do |img|
          img_s = img.gsub("#{Dir.pwd}/", '').pastel(:yellow)
          puts "  - #{img_s}"
        end
        puts

        exit unless prompt.yes? "#{'Are you sure?'.pastel(:bright_yellow)} Blur!? #{'(Destructive)'.pastel(:red)}"

        start(filter, size)
      end

      def self.start(filter, size)
        images.each do |img|
          LogBot.info('Image Blur', img)
          process(img, filter, size)
        end
      end

      def self.process(file, filter, size)
        image = MiniMagick::Image.open(file)

        # Ignore if the same
        return if image.width == size.split('x', 2).first.to_i

        LogBot.info('Image Blur', "Blurring: #{file}")
        image.filter filter
        image.blur size
        image.write file
      end

      def self.images
        Dir["#{Dir.pwd}/**/*.png"]
      end

      def self.prompt
        TTY::Prompt.new(active_color: :cyan, interrupt: :exit)
      end
      # =========================================================
    end
    # =========================================================
  end
end
