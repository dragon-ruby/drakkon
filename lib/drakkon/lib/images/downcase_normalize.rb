module Drakkon
  module Images
    # This downcases all the things
    module DowncaseNormalize
      def self.run!(_args = nil)
        puts <<~HELP
          - Run in the directory you wish to rename images
          - Downcase
          - Normalize
          	- To Underscores:
          		- Space
          		- Perentheses
          		- Dash
          - Delete Final Chars
          	- If Underscore
          Current Directory;
          	#{Dir.pwd.pastel(:yellow)}

               Images:
        HELP

        images.each do |img|
          img_s = img.gsub("#{Dir.pwd}/", '').pastel(:yellow)
          puts "  - #{img_s}"
        end
        puts

        exit unless prompt.yes? "#{'Are you sure?'.pastel(:bright_yellow)} Rename!? #{'(Destructive)'.pastel(:red)}"

        start
      end

      def self.start
        images.each do |img|
          process(img)
        end
      end

      # Image file name normalization. Downcase and replace spaces with underscore
      def self.process(file)
        new_name = File.basename(file).downcase.gsub(' ', '_').gsub('(', '_').gsub(')', '_').gsub('-', '_')
        path = File.dirname(file)

        # Remove Final Underscore last char of basename is an underscore
        ext = File.extname(new_name)
        basename = File.basename(new_name, ext)
        if basename[-1] == '_' && basename.length > 1
          new_name = basename[0..-2]
          new_name += ext
        end

        # Skip if the same
        return if new_name == File.basename(file)

        LogBot.info('Image Downcase Normalize', "Old: #{File.basename(file)}, New: #{new_name}")
        FileUtils.mv(file, "#{path}/#{new_name}")
      end

      def self.images
        Dir["#{Dir.pwd}/*.png"] + Dir["#{Dir.pwd}/*.PNG"]
      end

      def self.prompt
        TTY::Prompt.new(active_color: :cyan, interrupt: :exit)
      end
      # =========================================================
    end
    # =========================================================
  end
end
