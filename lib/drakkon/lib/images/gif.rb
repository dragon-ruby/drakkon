module Drakkon
  module Images
    # General Image Index Helper
    module GifSplit
      def self.run!(args = [])
        image = if args.empty?
                  prompt.select('What gif to split?', images, filter: true)
                else
                  args.first
                end

        png_image = "#{Dir.pwd}/#{image}.png"
        image = "#{Dir.pwd}/#{image}.gif"

        unless File.exist?(image)
          LogBot.fatal('Split', "Unable to find: '#{image}'")
          exit 1
        end

        puts <<~HELP
                    Usage [file]
                    - Modify one gif into a directory of split images


          Split:
              Current Directory: #{Dir.pwd.pastel(:yellow)}

          Gif to Split: #{image}
        HELP

        puts

        exit unless prompt.yes? "#{'Are you sure?'.pastel(:bright_yellow)} Split!? #{'(Destructive)'.pastel(:red)}"

        process(image, png_image)
      rescue TTY::Reader::InputInterrupt
        exit 0
      end

      def self.process(file, png_image)
        LogBot.info('Gif Split', file)
        convert = MiniMagick::Tool::Convert.new
        convert << file
        convert << png_image
        convert.call
      end

      def self.images
        Dir["#{Dir.pwd}/*.gif"].map do |x|
          File.basename(x, '.gif')
        end.sort
      end

      def self.prompt
        TTY::Prompt.new(active_color: :cyan, interrupt: :exit)
      end
      # =========================================================
    end
    # =========================================================
  end
end
