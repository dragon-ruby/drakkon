module Drakkon
  module Images
    # Dirty Compression.. Bad Idea?
    module Compress
      def self.run!(_args = [])
        puts <<~HELP
                    Usage
                    - Run in the directory you wish to modify the images
                    - Converts the Images to Webp and then back to PNG
          Images:
        HELP

        images.sort.each do |img|
          img_s = img.gsub("#{Dir.pwd}/", '').pastel(:yellow)
          puts "  - #{img_s}"
        end
        puts

        exit unless prompt.yes? "#{'Are you sure?'.pastel(:bright_yellow)} Compress!? #{'(Destructive)'.pastel(:red)}"

        start
      end

      def self.start
        images.each do |img|
          LogBot.info('Image Compress', img)
          process(img)
        end
      end

      def self.process(file)
        basename = File.basename(file, '.png')
        path = File.dirname(file)

        # Write Webp
        image = MiniMagick::Image.open(file)
        image.format 'webp'
        image.write "#{path}/#{basename}.webp"

        # Write back to PNG
        image = MiniMagick::Image.open("#{path}/#{basename}.webp")
        image.format 'png'
        image.write "#{path}/#{basename}.png"

        # Delete Temp
        FileUtils.rm("#{path}/#{basename}.webp")
      end

      def self.images
        Dir["#{Dir.pwd}/*.png"]
      end

      def self.prompt
        TTY::Prompt.new(active_color: :cyan, interrupt: :exit)
      end
      # =========================================================
    end
    # =========================================================
  end
end
