module Drakkon
  module Images
    # General Image Index Helper
    module SpriteSheetCombine
      def self.run!(args = [])
        # All images exist?
        unless images.all? { |x| File.exist?(x) } ||
               LogBot.fatal('SpriteSheetCombine', 'Invalid Images - Files must be numbered. e.g. 1.png, 2.png, 3.png')
          exit 1
        end

        img_name = if args.empty?
                     prompt.ask('New File Name? (e.g. rawr): ')
                   else
                     args.shift
                   end

        grid = if args.empty?
                 prompt.ask('Grid? (WxH: e.g. 1x1, blank will leave up to ImageMagick): ')
               else
                 args.shift
               end

        # Safety
        img_name ||= 'rawr'

        puts <<~HELP
                  Usage
                  - Run in the directory you wish to modify the images
                  - Images must be numbered so that the spritesheet can be created correctly

          Images:
        HELP

        images.each do |img|
          img_s = img.gsub("#{Dir.pwd}/", '').pastel(:yellow)
          puts "  - #{img_s}"
        end

        puts <<~OPTIONS
          Image Target: #{img_name.inspect}, Grid: #{grid.inspect}
          Current Directory;
          	#{Dir.pwd.pastel(:yellow)}
        OPTIONS

        puts

        exit unless prompt.yes? "#{'Are you sure?'.pastel(:bright_yellow)} Combine?"

        process(img_name, grid)
      rescue TTY::Reader::InputInterrupt
        exit 0
      end

      def self.process(img_name, grid)
        LogBot.info('Image Spritesheet', img_name)

        # Find Largest
        img = dimensions(images)
        img_name += '.png'

        LogBot.info('Image Spritesheet', "Dimensions: #{img[:w]}x#{img[:h]}")

        montage = MiniMagick::Tool::Montage.new
        images.each do |imgn|
          montage << imgn
        end

        montage.geometry "#{img.w}x#{img.h}+0+0"
        montage.tile grid unless grid.nil?
        montage.background 'none'
        montage << "#{Dir.pwd}/#{img_name}"

        montage.call
      end

      def self.dimensions(_file)
        # img = MiniMagick::Image.open(file)

        # {
        #   w: img.width,
        #   h: img.height
        # }

        index = images.map do |img|
          image_size(img)
        end

        w = index.max_by(&:w).w
        h = index.max_by(&:h).h

        { w: w, h: h }
      end

      def self.image_size(file)
        # LogBot.info('Image Biggest', file)
        img = MiniMagick::Image.open(file)

        {
          w: img.width,
          h: img.height
        }
      end

      def self.images
        @images ||= sorted_images
      end

      def self.sorted_images
        numbers = Dir["#{Dir.pwd}/*.png"].map do |x|
          File.basename(x, '.png').to_i
        end

        numbers.sort.map { |x| "#{x}.png" }
      end

      def self.prompt
        TTY::Prompt.new(active_color: :cyan, interrupt: :exit)
      end
      # =========================================================
    end
    # =========================================================
  end
end
