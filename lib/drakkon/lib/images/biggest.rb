module Drakkon
  module Images
    # General Image Index Helper
    module Biggest
      def self.run!(_args = [])
        start
      end

      def self.start
        LogBot.info('Image Biggest Index')
        index = images.map do |img|
          process(img)
        end

        img_w = index.max_by(&:w)
        img_h = index.max_by(&:h)

        puts "Largest Sizes: { w: #{img_w.w}, h: #{img_h.h} }"
        puts "  Width:  #{img_w}"
        puts "  Height: #{img_h}"
      end

      def self.process(file)
        # LogBot.info('Image Biggest', file)
        img = MiniMagick::Image.open(file)

        {
          file: File.basename(file),
          w: img.width,
          h: img.height
        }
      end

      def self.images
        Dir["#{Dir.pwd}/**/*.png"]
      end

      def self.prompt
        TTY::Prompt.new(active_color: :cyan, interrupt: :exit)
      end
      # =========================================================
    end
    # =========================================================
  end
end
