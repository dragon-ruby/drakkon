module Drakkon
  module Images
    # General Entry Point For Images Subcommand
    module CLI
      def self.run!(args = [])
        args ||= []
        cmd = if args.empty?
                nil
              else
                args.shift.to_sym
              end
        start(cmd, args)
      end

      # rubocop:disable Metrics/CyclomaticComplexity
      def self.start(cmd, args)
        case cmd
        when :list then Images::List.run!(args)
        when :biggest then Images::Biggest.run!(args)
        when :white then Images::White.run!(args)
        when :resize then Images::Resize.run!(args)
        when :shift then Images::Shift.run!(args)
        when :roll then Images::Roll.run!(args)
        when :scale then Images::Scale.run!(args)
        when :flip_flop then Images::FlipFlop.run!(args)
        when :rotate then Images::Rotate.run!(args)
        when :canvas then Images::Canvas.run!(args)
        when :desaturate then Images::Desaturate.run!(args)
        when :blur then Images::Blur.run!(args)
        when :split then Images::SplitSpriteSheet.run!(args)
        when :combine then Images::SpriteSheetCombine.run!(args)
        when :double then Images::Double.run!(args)
        when :trim then Images::Trim.run!(args)
        when :gif_split then Images::GifSplit.run!(args)
        when :sepia then Images::Sepia.run!(args)
        when :modulate then Images::Modulate.run!(args)
        when :layers then Images::Layers.run!(args)
        when :hue_modulate then Images::HueModulate.run!(args)
        when :bulk_rename then Images::BulkRename.run!(args)
        when :downcase_norm then Images::DowncaseNormalize.run!(args)
        when :text_to_image then Images::TextToImage.run!(args)
        when :compress then Images::Compress.run!(args)
        when :index then Images::Index.run!(force: true)
        when :exit then exit(0)
        else
          start(menu, args)
        end
      rescue TTY::Reader::InputInterrupt
        exit 0
      end
      # rubocop:enable Metrics/CyclomaticComplexity

      # rubocop:disable Metrics/BlockLength
      def self.menu
        prompt.select('Image Helpers:', filter: true, per_page: 20) do |menu|
          menu.choice name: 'Listing; Filter', value: :list
          menu.choice name: 'Biggest: Find largest dimensions [biggest]', value: :biggest
          menu.choice name: 'Convert alpha to white [white]', value: :white
          menu.choice name: 'Resize WxH [resize]', value: :resize
          menu.choice name: 'Scale [scale]', value: :scale
          menu.choice name: 'Shift (Splice/Gravity) [shift]', value: :shift
          menu.choice name: 'Roll [roll]', value: :roll
          menu.choice name: 'Flip/Flop [flip_flop]', value: :flip_flop
          menu.choice name: 'Rotate [rotate]', value: :rotate
          menu.choice name: 'Canvas (Change/Retain) [canvas]', value: :canvas
          menu.choice name: 'Desaturate (Grey Colorspace) [desaturate]', value: :desaturate
          menu.choice name: 'Blur [blur]', value: :blur
          menu.choice name: 'Split SpriteSheet [split]', value: :split
          menu.choice name: 'Create SpriteSheet [combine]', value: :combine
          menu.choice name: 'Double Image [double]', value: :double
          menu.choice name: 'Trim Transparency (:trim]', value: :trim
          menu.choice name: 'Gif Split [gif_split]', value: :gif_split
          menu.choice name: 'Sepia [sepia]', value: :sepia
          menu.choice name: 'Modulate [modulate]', value: :modulate
          menu.choice name: 'Layers [layers]', value: :layers
          menu.choice name: 'Bulk Rename [bulk_rename]', value: :bulk_rename
          menu.choice name: 'Downcase Normalize [downcase_norm]', value: :downcase_norm
          menu.choice name: 'Hue Modulate [hue_modulate]', value: :hue_modulate
          menu.choice name: 'TextToImage [text_to_image]', value: :text_to_image
          menu.choice name: 'Compress (file size) [compress]', value: :compress
          menu.choice name: 'Index (Re-run Index) [index]', value: :index
          menu.choice name: 'exit', value: :exit
        end
      rescue TTY::Reader::InputInterrupt
        exit 0
      end
      # rubocop:enable Metrics/BlockLength

      def self.prompt
        TTY::Prompt.new(active_color: :cyan, interrupt: :exit)
      end
      # =========================================================
    end
    # =========================================================
  end
end
