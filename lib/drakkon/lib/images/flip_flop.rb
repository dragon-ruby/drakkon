module Drakkon
  module Images
    # General Image Index Helper
    module FlipFlop
      def self.run!(args = [])
        # X
        horizontal = if args.empty?
                       prompt.yes?('Flip Horizontally?')
                     else
                       v = args.shift.downcase
                       %w[yes y].include?(v)
                     end

        # Y
        vertical = if args.empty?
                     prompt.yes?('Flip Vertically?')
                   else
                     v = args.shift.downcase
                     %w[yes y].include?(v)
                   end

        puts <<~HELP
                    Usage [horizontal] [vertical]
                    - Run in the directory you wish to modify the images
                    - Flip/Flop the images via MiniMagick
                    - y / yes / Y / Yes for affirmative args




          Horizontal: #{horizontal}
          Vertical: #{vertical}
                    Current Directory;
                      #{Dir.pwd.pastel(:yellow)}

          Images:
        HELP

        images.sort.each do |img|
          img_s = img.gsub("#{Dir.pwd}/", '').pastel(:yellow)
          puts "  - #{img_s}"
        end
        puts

        exit unless prompt.yes? "#{'Are you sure?'.pastel(:bright_yellow)} Flip/Flop!? #{'(Destructive)'.pastel(:red)}"

        start(horizontal, vertical)
      end

      def self.start(horizontal, vertical)
        images.each do |img|
          LogBot.info('Image Flip Flop', img)
          process(img, horizontal, vertical)
        end
      end

      def self.process(file, horizontal, vertical)
        image = MiniMagick::Image.open(file)

        image.flip if vertical
        image.flop if horizontal
        image.write file
      end

      def self.images
        Dir["#{Dir.pwd}/**/*.png"]
      end

      def self.prompt
        TTY::Prompt.new(active_color: :cyan, interrupt: :exit)
      end
      # =========================================================
    end
    # =========================================================
  end
end
