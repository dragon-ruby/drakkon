module Drakkon
  module Images
    # Make some img files from fonts
    module TextToImage
      def self.run!(args = [])
        text = if args.empty?
                 prompt.ask('Text? (e.g. rawr): ')
               else
                 args.shift
               end

        size = if args.empty?
                 24
               else
                 args.shift
               end

        color = if args.empty?
                  'white'
                else
                  args.shift
                end

        file = if args.empty?
                 text.gsub(' ', '').downcase
               else
                 args.shift
               end

        file.gsub!(/[^0-9A-Za-z.-]/, '_')

        # Remove Dashes
        file.gsub!(/-+/, '_')

        font = if args.empty?
                 font_prompt
               else
                 args.shift
               end

        puts <<~HELP
                  Usage [text] [size=24] [color=white] [file=text] [font]
                  - Will look for fonts directory
                  - Generates via MiniMagick on transparent background


          Text: #{text}
          Size: #{size}
          Color: #{color}
          File: #{file}
          Font: #{font}
        HELP

        puts

        exit unless prompt.yes? 'Are you sure?'.pastel(:bright_yellow).to_s

        generate(font: font, text: text, size: size, color: color, file: file)
      rescue TTY::Reader::InputInterrupt
        exit 0
      end

      def self.font_prompt
        list = Dir['*.ttf', '*.otf', 'fonts/*']

        prompt.select('Select Font:', filter: true) do |menu|
          list.each do |font|
            menu.choice name: font
          end
        end
      end

      def self.generate(font:, text:, size:, color:, file:)
        LogBot.info('TextToImage', file)
        MiniMagick::Tool::Convert.new do |convert|
          convert.background 'none'
          convert.fill color
          convert.font font
          convert.pointsize size
          convert << "label:#{text}"
          convert << "#{file}.png"
        end
      end

      def self.prompt
        TTY::Prompt.new(active_color: :cyan, interrupt: :exit)
      end
      # =========================================================
    end
    # =========================================================
  end
end
