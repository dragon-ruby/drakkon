module Drakkon
  module Images
    # General Image Index Helper
    # https://www.imagemagick.org/Usage/color_mods/#modulate_hue
    module HueModulate
      def self.run!(args = [])
        # Amount
        amount = if args.empty?
                   puts 'Amount Syntax: brightness,saturation,hue'
                   prompt.ask('Amount? (0..200) 100 is no change: ')
                 else
                   args.shift
                 end

        puts <<~HELP
          Usage
          - Run in the directory you wish to modify the images
          - Hue Modulates images via MiniMagick

          Hue Modulate Amount: #{amount}
            Current Directory;
              #{Dir.pwd.pastel(:yellow)}

          Current Directory;
            #{Dir.pwd.pastel(:yellow)}

          Images:
        HELP

        images.sort.each do |img|
          img_s = img.gsub("#{Dir.pwd}/", '').pastel(:yellow)
          puts "  - #{img_s}"
        end
        puts

        unless prompt.yes? "#{'Are you sure?'.pastel(:bright_yellow)} Hue Modulate!? #{'(Destructive)'.pastel(:red)}"
          exit
        end

        start(amount)
      end

      def self.start(amount)
        images.each do |img|
          LogBot.info('Image Mod', img)
          process(img, amount)
        end
      end

      def self.process(file, amount)
        image = MiniMagick::Image.open(file)

        image.combine_options do |c|
          c.modulate "100,100,#{amount}"
        end

        image.write(file)
      end

      def self.images
        Dir["#{Dir.pwd}/**/*.png"]
      end

      def self.prompt
        TTY::Prompt.new(active_color: :cyan, interrupt: :exit)
      end
      # =========================================================
    end
    # =========================================================
  end
end
