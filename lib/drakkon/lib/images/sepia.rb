module Drakkon
  module Images
    # General Image Index Helper
    # https://imagemagick.org/script/command-line-options.php#sepia-tone
    module Sepia
      def self.run!(args = [])
        # Amount
        amount = if args.empty?
                   prompt.ask('Sepia Threshold? (e.g. 80): ')
                 else
                   args.shift
                 end

        puts <<~HELP
          Usage
          - Run in the directory you wish to modify the images
          - Applies a special effect to the image, similar to the effect achieved in a photo darkroom by sepia toning.

          Sepia: Amount: #{amount}
          Current Directory;
            #{Dir.pwd.pastel(:yellow)}

          Images:
        HELP

        images.sort.each do |img|
          img_s = img.gsub("#{Dir.pwd}/", '').pastel(:yellow)
          puts "  - #{img_s}"
        end
        puts

        exit unless prompt.yes? "#{'Are you sure?'.pastel(:bright_yellow)} Sepia!? #{'(Destructive)'.pastel(:red)}"

        start(amount)
      end

      def self.start(amount)
        images.each do |img|
          LogBot.info('Image Sepia', img)
          process(img, amount)
        end
      end

      def self.process(file, amount)
        convert = MiniMagick::Tool::Convert.new
        convert << file
        convert.colorspace('RGB')
        convert.sepia_tone("#{amount}%")
        convert << file
        convert.call
      end

      def self.images
        Dir["#{Dir.pwd}/**/*.png"]
      end

      def self.prompt
        TTY::Prompt.new(active_color: :cyan, interrupt: :exit)
      end
      # =========================================================
    end
    # =========================================================
  end
end
