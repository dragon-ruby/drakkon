# Top level namespace
module Drakkon
  # Replace Colorize with Pastel Monkey Patch String Shim
  # https://github.com/piotrmurach/pastel#features

  # Looping in  to allow disabling of Color
  module Color
    def self.pastel
      @pastel ||= Pastel.new
    end

    def self.decorate(args)
      pastel.decorate(*args)
    end
  end
end

# Monkey Patch
class String
  def pastel(*args)
    Drakkon::Color.decorate(args.unshift(self))
  end

  def unpastel
    Drakkon::Color.pastel.strip self
  end

  def snake_case
    return downcase if match(/\A[A-Z]+\z/)

    gsub(/([A-Z]+)([A-Z][a-z])/, '\1_\2')
      .gsub(/([a-z])([A-Z])/, '\1_\2')
      .downcase
  end
end

# Monkey Patch
class Symbol
  def pastel(*args)
    Drakkon::Color.decorate(args.unshift(to_s))
  end
end

# Monkey Patch
class Array
  def pastel(*args)
    Drakkon::Color.decorate(args.unshift(to_s))
  end
end
