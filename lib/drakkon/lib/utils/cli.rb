module Drakkon
  module Utils
    # General Entry Point For Images Subcommand
    module CLI
      def self.run!(args = [])
        args ||= []
        cmd = if args.empty?
                nil
              else
                args.shift.to_sym
              end
        start(cmd, args)
      end

      def self.start(cmd, args)
        case cmd
        when :normalize then Utils::DowncaseNormalize.run!(args)
        else
          start(menu, args)
        end
      end

      def self.menu
        prompt.select('Utilties:', filter: true, per_page: 20) do |menu|
          menu.choice name: 'Rename Files - Normalize [normalize]', value: :normalize
        end
      rescue TTY::Reader::InputInterrupt
        exit 0
      end

      def self.prompt
        TTY::Prompt.new(active_color: :cyan, interrupt: :exit)
      end
      # =========================================================
    end
    # =========================================================
  end
end
