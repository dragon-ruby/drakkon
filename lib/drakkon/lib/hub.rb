module Drakkon
  # General Info Helpers
  module Hub
    # Ensrue Setup
    def self.init
      # rubocop:disable Lint/NonAtomicFileOperation
      # Ensure Home Directory; It is neccessary stupid rubocop
      Dir.mkdir(Hub.dir) unless Dir.exist?(Hub.dir)
      # rubocop:enable Lint/NonAtomicFileOperation

      setup unless File.exist?(config_file)
    end

    def self.setup
      LogBot.info('Hub', 'Init Config')
      opts = config_defaults

      # TODO: Be better, this is lazy...
      opts.each do |key, value|
        Hub.update(key, value)
      end
    end

    def self.config_defaults
      {
        versions: [],
        skeletons: {}
      }
    end

    # Combine Dir Location
    def self.dir
      "#{Dir.home}/.drakkon"
    end

    def self.config_file
      "#{dir}/config.json"
    end

    def self.version?(version)
      versions.include? version
    end

    def self.versions
      config.versions
    end

    def self.version_installed?
      versions.any?
    end

    def self.version_latest
      list = versions.map { |v| v.split('.').map(&:to_i) }
      latest = list.sort.reverse.max
      latest.join('.')
    end

    def self.versions_sorted
      list = versions.map { |v| v.split('.').map(&:to_i) }
      list.sort.reverse.map { |v| v.join('.') }
    end

    def self.version_add(version)
      config[:versions].push version

      update(:versions, config[:versions])
    end

    def self.config
      # Write Default
      File.write(config_file, JSON.pretty_generate({})) unless File.exist?(config_file)

      @config ||= JSON.parse(File.read(config_file), { symbolize_names: true })

      # TODO: Optimize this a bit more
      config_defaults.each do |key, value|
        # ||= Is problematic with falses
        @config[key] = value unless @config.key?(key)
      end

      @config
    end

    def self.update(key, value)
      config[key] = value
      write
    end

    def self.delete(key)
      config.delete key
      write
    end

    def self.write
      # LogBot.debug('Hub', "Writing Config: #{config_file}")
      File.write(config_file, JSON.pretty_generate(config))
    end

    def self.log_file
      "#{dir}/output.log"
    end

    # =========================
    # Template Helpers
    # =========================
    def self.skeletons?
      !config.skeletons.empty?
    end

    def self.skeletons
      config.skeletons
    end

    # ======================================================================
  end
  # ======================================================================
end
