module Drakkon
  module Sounds
    # General Sound Index Helper
    module Index
      def self.index
        @index ||= {}

        @index
      end

      def self.context
        @context ||= Dir.pwd

        @context
      end

      def self.catalog
        # Better way to do this?
        @catalog ||= Hash.new { |hash, key| hash[key] = Hash.new(&hash.default_proc) }

        @catalog
      end

      def self.file_list
        Dir["#{sounds_directory}/**/*"].select { |file| File.file?(file) }
      end

      def self.digest
        Digest::MD5.hexdigest(file_list.map { |x| Digest::MD5.file(x).hexdigest }.join)
      end

      def self.sounds_directory
        "#{context}/sounds"
      end

      def self.run!(force: false, dir: nil)
        @context = dir || Dir.pwd

        # Create Directory if sprites directory is missing
        FileUtils.mkdir_p(sounds_directory)

        if Settings.config[:sound_digest] == digest && File.exist?("#{context}/app/drakkon/sound_index.rb")
          LogBot.info('Sounds Index', 'Nothing New')
          return unless force
        end

        build_index

        Settings.update(:sound_digest, digest)

        File.write("#{context}/app/drakkon/sound_index.rb", result)
      end

      def self.build_index
        check sounds_directory
      end

      def self.result
        <<~RB
             module Drakkon
               module Sounds
                 def self.index
                   #{index.inspect}
                 end

          def self.catalog
                   #{catalog.inspect}
                 end

               end
             end
        RB
      end

      # Recursively Go through
      def self.check(dir = nil)
        LogBot.info('Sound Index', "Check: #{dir}")

        # Collect Sounds
        list = file_list

        # Ignore Empties
        return if list.empty?

        # Do other things
        Dir["#{dir}/*"].each do |file|
          if File.directory?(file)
            check(file)
            next
          end

          process(file)
        end

        :done
      end

      def self.process(file = nil)
        # Safety
        return if file.nil?

        full_name = file.gsub(sounds_directory, '')[1..].gsub(File.extname(file), '')

        name = File.basename(file, File.extname(file))
        path = file.gsub(sounds_directory, '')[1..]
        index[full_name] = path

        details = { path: full_name }
        tag = WahWah.open(file)
        details[:duration_s] = tag.duration
        details[:duration_tick] = tag.duration * 60

        # Catalog
        catalog_list = path.split('/')[0..-2]
        catalog_location = []
        catalog_list.each do |idx|
          catalog_location.push idx
          catalog.dig(*catalog_location)
        end
        catalog.dig(*catalog_list)[name] = details
      end
      # =========================================================
    end
    # =========================================================
  end
end
