module Drakkon
  # Project Specific Config Settings
  module Settings
    def self.init?
      if File.directory?(config_file)
        LogBot.fatal('Settings', "Conflicting Config / Directory #{config_file}")
        exit 0
      end

      File.exist?(config_file)
    end

    # Helper to exit if project isn't ready for drakkon usage
    def self.ready?
      return false if init?

      LogBot.warn('Setup', "Drakkon not configured. Run #{'init'.pastel(:green)} first")

      exit 1
    end

    def self.check_version!
      return if Hub.version?(version)

      LogBot.warn('Setup', "DragonRuby #{version} not installed.")
      LogBot.warn('Setup', 'Select a version to use:')

      Version.set

      exit(1) unless Hub.version?(version)
    end

    def self.prompt
      TTY::Prompt.new(active_color: :cyan, interrupt: :exit)
    end

    def self.version
      config.version
    end

    def self.gems
      config[:gems]
    end

    def self.platforms?
      !config[:platforms].empty?
    end

    def self.platforms
      config[:platforms]
    end

    def self.app_dir
      "#{Dir.pwd}/app"
    end

    def self.drakkon_dir
      "#{Dir.pwd}/app/drakkon"
    end

    def self.image_index?
      config[:image_index]
    end

    def self.font_index?
      config[:font_index]
    end

    def self.sound_index?
      config[:sound_index]
    end

    def self.bundle_compile?
      config[:bundle_compile]
    end

    def self.manifest_compile?
      config[:manifest_compile]
    end

    def self.manifest?
      config[:manifest]
    end

    def self.config_file
      "#{Dir.pwd}/.drakkon"
    end

    def self.image_magick_installed?
      # Use Minimagick's utilities to check if ImageMagick is installed
      # https://github.com/minimagick/minimagick/pull/193
      !MiniMagick::Utilities.which('convert').nil?

      # MiniMagick::Image.open('metadata/icon.png')
    rescue MiniMagick::Invalid, Errno::ENOENT
      false
    end

    def self.enable_image_index?
      return false unless image_magick_installed?

      # TODO: Determine if this should default to True?
      return true if config[:image_index].nil?

      config[:image_index]
    end

    def self.config_defaults
      # Defaults
      @config[:platforms] ||= []
      @config[:version] ||= Hub.version_latest
      @config[:image_index] = enable_image_index?
      @config[:manifest] = true if @config[:manifest].nil?
      @config[:bundle_compile] = true if @config[:bundle_compile].nil?
      @config[:manifest_compile] = false if @config[:manifest_compile].nil?
      @config[:font_index] = true if @config[:font_index].nil?
      @config[:sound_index] = true if @config[:sound_index].nil?
      @config[:gems] ||= {}
    end

    def self.config
      # Write Default
      File.write(config_file, JSON.pretty_generate({})) unless File.exist?(config_file)

      if @config.nil?
        @config ||= JSON.parse(File.read(config_file), { symbolize_names: true })
        config_defaults
        write
      end

      @config
    end

    def self.update(key, value)
      config[key] = value
      write
    end

    def self.delete(key)
      config.delete key
      write
    end

    def self.write
      # LogBot.debug('Settings', "Writing Config: #{config_file}")
      File.write(config_file, JSON.pretty_generate(config))
    end

    def self.menu
      prompt.select('Change Settings:', filter: true) do |menu|
        menu.choice name: 'platforms', value: :platforms
        menu.choice name: 'version', value: :version
        menu.choice name: 'image_index', value: :image_index
        menu.choice name: 'font_index', value: :font_index
        menu.choice name: 'sound_index', value: :sound_index
        menu.choice name: 'manifest', value: :manifest
        menu.choice name: 'manifest_compile', value: :manifest_compile
        menu.choice name: 'bundle_compile', value: :bundle_compile
        menu.choice name: 'exit', value: :exit
      end
    rescue TTY::Reader::InputInterrupt
      exit 0
    end

    def self.menu_do(arg)
      case arg
      when :platforms
        Settings.update(:platforms, Build.platform_setup)
      when :version
        Version.set
      when :image_index
        Settings.update(:image_index, prompt.yes?("Index Images? (Enabled: #{image_index?})"))
      when :manifest
        Settings.update(:image_index, prompt.yes?("Write Manifest? (Enabled: #{manifest?})"))
      when :bundle_compile
        Settings.update(:bundle_compile,
                        prompt.yes?("Create Bundle / Compile Gems? (Enabled: #{config[:bundle_compile]})"))
      when :manifest_compile
        Settings.update(:manifest_compile,
                        prompt.yes?("Bundle Manifest On Build? (Enabled: #{config[:manifest_compile]})"))
      when :font_index
        Settings.update(:font_index, prompt.yes?("Index Fonts? (Enabled: #{config[:font_index]})"))
      when :sound_index
        Settings.update(:font_index, prompt.yes?("Index Sounds? (Enabled: #{config[:sound_index]})"))
      when :exit
        exit 0
      end
    end

    # ======================================================================
  end
  # ======================================================================
end
