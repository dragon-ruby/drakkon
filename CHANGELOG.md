# 0.0.10

## General

- Settings Fixes 
- Trigger new selected version if configured version doesn't exist
- Don't overwrite files during init
  - Copy of required metadata / icon
- Check for ImageMagick installation (Be a bit more open on exceptions)
  - Use ImageMagick Utils
- Fix gem install if not ready
- Remove Sinatra requires
- Allow for gem empty modules
- Windows Fixes!
  - Update Command calls to use IO.popen
  - Switch from `unzip` to RubyZip
- Fix Permissions with RubyZip
- Adding platformer helpers for windows/linux Path Separators (I suspect more platform pivots will be needed as well)
- Fix annoying exception when interupt prompt
- Comment Code Clean Up
- Bump Gem Versions (ImageMagick, MiniMagick bugs)



## Utils

- Add general utils commands
- First general file normalization Util Command

## Run

- Pass arguments to run command

## Skeleton 

- Better Missing file errors

## Manifest

- Expand out into require statements (rather than array processed at runtime)
- Write manifest if file is missing regardless of digest

## Bundle

- Write bundle if file is missing regardless of digest

## Font Index

- Write index if file is missing regardless of digest
- Support sub directories

## Sounds Index

- Add Sounds Index! 
- Add WahWah for duration values (duration_s, duration_tick)

## Images

- Add in square braces what the short cut is from the CLI [roll] => drakkon images roll
- Make Directories if missing
- Write index if file is missing regardless of digest
- Fix animation/image processing where sprites weren't being processed
- Fix text to image if there are dashes
- Add Hue Modulation
- Update labels to make easier to find / Include Bulk Rename
- Add Double: Double up an image
- Add `catalog_dig` which will automatically split and dig paths 'long/path' => 'long','path'
- Fix/Switch Trim to Convert Command
- Add Dirty PNG > Webp > PNG compression
- Update Biggest to include more info (which files)
- Update split with floats
- Add resize prompt '!' to force the size

## Web

- Update Commands to Match new run
- Follow output
- Graceful exit on Ctrl-C

## Manifest Compile

- Compile automatically generated manifest on build

---

# 0.0.8

## General

- Lint
- Fix dumb conditional assignment
- Dependencies Clean Up
  - Removing OJ to Simplify dependencies
  - Remove Sinatra / Faker

## Bundle

- Make Bundle work without gems
- Change internals to have more metadata on files
- Adding an option to compile the bundle or not `bundle_compile`
  - This will spread safe_file names in the same load order into the `unbundle` directory.
  - Will use a 'safe_name' which removes directory pathing to underscores

## Font Index

- Introducing font index (pesky full file font paths for things with different exts)

```ruby
module Drakkon
  module Fonts
    def self.index
      {
        'LittleGuy' => 'LittleGuy.ttf',
        'NanoPlus' => 'NanoPlus.otf'
      }
  end
end
```

---

# 0.0.7

## Bundle

- Show an error for any missing files but continue to bundle

## Images

- New TextToImage helper - Generate PNG's from font files

# 0.0.6

- More ruby 3.0 Hash Syntax fixes

## Init

- Prompt for missing dragonruby install

# 0.0.5

- Ruby 3.0 Hash syntax fixes

# 0.0.4

## General

- Update `tty-prompt` to exit rather than throw exceptions

## Binary

- Add Drakkon version to the flair

## Gem

- Prompt for install path / nicer error handling
- Lower Required Ruby Version to 3.0 (Nothing is particular specific to Ruby versions, and 3+ should be safe)

## Install

- Prompt for installation rather than throwing an exception

# 0.0.3

- Preparing for Initial Release
- Clean up of all my random comments
- Rubocop Initial clean up
