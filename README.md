# Drakkon

Runtime Helper for DragonRuby Games!

# Requirements

- Lots

### Ubuntu

I'm Sure there's something

## Installation

```
gem install drakkon
```

## Usage

```
drakkon .
```

## Testing

```
bundle exec rake
```

## Release Process

- Update CHANGELOG
- Increment `release.rb`
- Create MR
- Merge to Master

```
# Build and Push
bundle exec rake build
gem push pkg/drakkon-1.x.x.gem
```

## TODO

- See Issues (Readme Todo's suck)

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
